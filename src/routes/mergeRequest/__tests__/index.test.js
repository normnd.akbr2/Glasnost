import {getRegisteredRoutes} from 'router';
import {getReducers} from 'store';

import * as index from '../index';

describe('MergeRequest index', () => {
    it('should export MergeRequest and reducers', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().MergeRequest).toBeDefined();
        expect(getReducers().mergeRequest).toBeDefined();
    });
});

import React from 'react';
import {Provider} from 'react-redux';

import {getRegisteredRoutes} from 'router';
import {getStarredRepositories} from '../selectors';
import {clearStarredRepositories} from '../actions';

const fakeStarredRepositories = [{id: 1}, {id: 2}];

describe('StarredRepositories', () => {
    let StarredRepositories;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        setStore(getStore());
    });

    beforeEach(() => {
        StarredRepositories = getRegisteredRoutes().StarredRepositories;

        store = require('store').getStore();
        store.dispatch({type: 'REPOSITORIES_RECEIVED', data: fakeStarredRepositories});
    });

    it('renders correctly the empty page', async () => {
        store.dispatch(clearStarredRepositories());
        const component = await asyncCreate(<Provider store={store}><StarredRepositories/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should call the functions when unmounting', async () => {
        const component = mount(<Provider store={store}><StarredRepositories/></Provider>);

        component.unmount();
        await waitForAsync();

        expect(getStarredRepositories(store.getState())).toMatchSnapshot();
    });

    it('should refresh the data', async () => {
        const component = mount(<Provider store={store}><StarredRepositories/></Provider>);
        await waitForAsync();
        component.update();

        const StarredRepositoriesList = component.find('RepositoriesListComponent');
        StarredRepositoriesList.props().refresh();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should request the repositories when onPress', async () => {
        const component = mount(<Provider store={store}><StarredRepositories/></Provider>);
        await waitForAsync();
        component.update();

        const StarredRepositoriesList = component.find('RepositoriesListComponent');
        StarredRepositoriesList.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });
});

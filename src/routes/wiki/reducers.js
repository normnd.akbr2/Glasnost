import {registerReducer} from 'store';

function wiki() {
    return {
        WIKI_RECEIVED: (state, {data}) => ({...state, wiki: data})
    };
}

registerReducer('wiki', wiki());

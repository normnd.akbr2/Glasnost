import React, {Component, memo} from 'react';
import {FlatList, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {setFooter, setHeaderLeftButton, setHeaderRightButton, MainFooter} from 'headerFooter';
import {getPreviousRouteName, navigate, registerRoute} from 'router';
import {getLastUpdatedTime} from 'utils';
import {
    FormattedText,
    ItemList,
    Tabs,
    Tag,
    Icon,
    AppContainer,
    EmptyData,
    LoadMore
} from 'elements';
import {getClosedIssues, getOpenIssues} from './selectors';
import {requestIssues} from './actions';
import {Footer} from '../repository/Footer';
import {MyFace} from '../repositories/MyFace';
import {Avatars} from './Avatars';

class IssuesComponent extends Component {
    state = {selectedTab: 'Open'};

    componentDidMount() {
        const {requestIssues, setFooter, personal, setHeaderRightButton, setHeaderLeftButton} = this.props;

        setFooter(this.footer());
        setHeaderLeftButton(this.headerLeftButton());
        setHeaderRightButton(this.headerRightButton());
        requestIssues(personal);
    }

    footer() {
        return this.props.personal ? <MainFooter/> : <Footer/>;
    }

    headerLeftButton() {
        const {personal, navigate} = this.props;

        return personal ? <MyFace/> : <Icon name={'angle-left'} size={25} style={{padding: 5}} onPress={() => navigate('Repository')}/>;
    }

    headerRightButton() {
        const {personal, navigate} = this.props;

        return !personal ? <Icon name={'issue-new'} size={30} onPress={() => navigate('NewIssue')}/> : null;
    }

    onScroll({nativeEvent}) {
        const {setFooter} = this.props;
        const currentOffset = nativeEvent.contentOffset.y;
        const diff = currentOffset - (this.offset || 0);

        if (diff < 0.3) {
            setFooter(this.footer());
        } else {
            setFooter();
        }

        this.offset = currentOffset;
    }

    render() {
        const {openIssues, closedIssues, requestIssues, personal, navigate} = this.props;
        const {selectedTab} = this.state;
        const issues = selectedTab === 'Open' ? openIssues : closedIssues;

        const tabs = [
            {id: 'Open', text: `Open (${_.size(openIssues)})`, onPress: () => this.setState({selectedTab: 'Open'})},
            {
                id: 'Closed',
                text: `Closed (${_.size(closedIssues) === 100 ? '99+' : _.size(closedIssues)})`,
                onPress: () => this.setState({selectedTab: 'Closed'})
            }
        ];

        return (
            <AppContainer>
                <Tabs tabs={tabs} selectedTab={selectedTab}/>
                <AppContainer>
                    {!_.size(issues) && <EmptyData/>}
                    <FlatList
                        onRefresh={() => requestIssues(personal, false)}
                        refreshing={false}
                        ListFooterComponent={<LoadMore entityName={'issues'} onPress={() => requestIssues(false)}/>}
                        keyExtractor={item => `${item.id}`}
                        onScroll={e => this.onScroll(e)}
                        data={issues}
                        renderItem={({item}) => <SingleIssue navigate={navigate} issue={item}/>}/>
                </AppContainer>
            </AppContainer>
        );
    }
}

function Labels({labels}) {
    return (
        <View style={{flexDirection: 'column'}}>
            {_.map(labels, (label, index) => (
                <View key={index} style={{marginBottom: 4}}>
                    <Tag key={index} text={_.truncate(label, {length: 15})}/>
                </View>
            ))}
        </View>
    );
}

function SingleIssue({navigate, issue}) {
    const {assignee, author, iid, project_id, labels} = issue;

    return (
        <ItemList onPress={() => navigate('Issue', {iid, projectId: project_id})}>
            <Avatars assignee={assignee} author={author}/>
            <Description issue={issue}/>
            <Labels labels={labels}/>
        </ItemList>
    );
}

function Description({issue}) {
    const {title, updated_at} = issue;
    const {unit, value} = getLastUpdatedTime(updated_at);

    return (
        <View style={{flexDirection: 'column', flex: 1}}>
            <FormattedText style={{flex: 1, fontWeight: 'bold'}}>{title}</FormattedText>
            <FormattedText>{`Last update: ${value} ${unit} ago`}</FormattedText>
        </View>
    );
}

const Issues = connect(state => ({
    openIssues: getOpenIssues(state),
    closedIssues: getClosedIssues(state),
    previousRouteName: getPreviousRouteName(state)
}), {
    requestIssues, setFooter, navigate, setHeaderRightButton, setHeaderLeftButton
})(memo(IssuesComponent));
registerRoute({Issues});

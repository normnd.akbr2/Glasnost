import React from 'react';
import {Provider} from 'react-redux';
import _ from 'lodash';
import moment from 'moment';

import {getRegisteredRoutes, getRouteName, navigate} from 'router';

import {getClosedIssues} from '../selectors';
import {clearIssues, requestIssues} from '../actions';

const fakeIssues = [
    {
        state: 'opened',
        title: 'someTitle1',
        updated_at: moment().toISOString,
        author: {
            name: 'someAuthor1',
            id: 5
        },
        assignee: {
            name: 'someAssignee1',
            id: 1
        },
        iid: 1,
        id: 1,
        project_id: 1,
        labels: ['someLabel1']
    },
    {
        state: 'closed',
        title: 'someTitle2',
        updated_at: moment().toISOString,
        author: {
            name: 'someAuthor2',
            id: 2
        },
        assignee: {
            name: 'someAssignee2',
            id: 2
        },
        iid: 2,
        id: 2,
        project_id: 1,
        labels: ['someLabel2']
    }
];

const otherIssue = [
    {
        state: 'opened',
        title: 'someTitle3',
        updated_at: moment().toISOString(),
        author: {
            name: 'someAuthor3',
            id: 7
        },
        assignee: {
            name: 'someAssignee3',
            id: 3
        },
        iid: 3,
        id: 3,
        project_id: 3,
        labels: ['someLabel3']
    }
];

describe('Issues', () => {
    let Issues;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../repository/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        Issues = getRegisteredRoutes().Issues;

        const {getStore} = require('store');
        store = getStore();

        // populate navigation's history
        store.dispatch(navigate('Repository'));
        store.dispatch(navigate('Glasnost'));
    });

    it('renders correctly the empty list', async () => {
        const component = await asyncCreate(<Provider store={store}><Issues/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should not fill the store because data was not retrieved', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: null});

        store.dispatch(requestIssues(false, true));
        await waitForAsync();
        expect(_.isEmpty(getClosedIssues(store.getState()))).toBeTruthy();
    });

    it('should fetch data from page0', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});

        store.dispatch(requestIssues(false, true));
        await waitForAsync();
        expect(_.isEmpty(getClosedIssues(store.getState()))).toBeFalsy();
    });

    it('should fetch personal data from page0', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});

        store.dispatch(requestIssues(true, true));
        await waitForAsync();
        expect(_.isEmpty(getClosedIssues(store.getState()))).toBeFalsy();
    });

    it('renders correctly the list with some issues', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});
        const component = await asyncCreate(<Provider store={store}><Issues/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders correctly the list with some personal issues', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});
        const component = await asyncCreate(<Provider store={store}><Issues personal={true}/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should open the opened tab', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});
        const component = mount(<Provider store={store}><Issues/></Provider>);
        await waitForAsync();
        component.update();

        const SingleTab = component.find('SingleTab').at(0);
        SingleTab.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should open the closed tab', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});
        const component = mount(<Provider store={store}><Issues/></Provider>);
        await waitForAsync();
        component.update();

        const SingleTab = component.find('SingleTab').at(1);
        SingleTab.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should refresh when pulling down the list', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});
        const component = mount(<Provider store={store}><Issues/></Provider>);
        await waitForAsync();
        component.update();

        const ScrollableAppContainer = component.find('ScrollableAppContainerComponent').at(0);
        ScrollableAppContainer.props().refresh();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should ask for more issues when clicking on the button', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});

        const component = mount(<Provider store={store}><Issues/></Provider>);
        await waitForAsync();

        onGet({url: /commits*/, data: otherIssue});
        const LoadMore = component.find('LoadMoreComponent');
        LoadMore.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should navigate to the single issue when clicking on it', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});
        const component = mount(<Provider store={store}><Issues/></Provider>);
        await waitForAsync();
        component.update();

        const SingleIssue = component.find('SingleIssue').find('TouchableOpacity').at(0);
        SingleIssue.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Issue');
    });

    it('renders correctly a lot of closed issues', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        const issues = [];

        for (let issueIdx = 0; issueIdx <= 100; issueIdx++) {
            issues.push({state: 'closed', id: issueIdx, iid: issueIdx});
        }
        onGet({url: /issues*/, data: issues});
        const component = await asyncCreate(<Provider store={store}><Issues/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should navigate to the author\'s profile when clicking on it', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});
        const component = mount(<Provider store={store}><Issues/></Provider>);
        await waitForAsync();
        component.update();

        const Author = component.find('CircledImageComponent').at(0);
        Author.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Profile');
    });

    it('should navigate to the assignee\'s profile when clicking on it', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});
        const component = mount(<Provider store={store}><Issues/></Provider>);
        await waitForAsync();
        component.update();

        const Assignee = component.find('CircledImageComponent').at(1);
        Assignee.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Profile');
    });

    it('should call the functions when unmounting', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssues});
        const component = mount(<Provider store={store}><Issues/></Provider>);
        await waitForAsync();
        component.update();

        store.dispatch(clearIssues());

        expect(_.isEmpty(getClosedIssues(store.getState()))).toBeTruthy();
    });
});

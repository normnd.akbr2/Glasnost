import {registerReducer} from 'store';

const repository = {
    REPOSITORY_RECEIVED: (state, {data}) => ({...state, repository: data}),
    CLEAR_REPOSITORY: state => ({...state, repository: {}}),
    FILELIST_RECEIVED: (state, {data}) => ({...state, fileList: data}),
    CLEAR_FILELIST: state => ({...state, fileList: []}),
    MEMBERS_RECEIVED: (state, {data}) => ({...state, members: data}),
    BLOB_RECEIVED: (state, {name, data}) => ({...state, blob: {name, blob: data}}),
    README_RECEIVED: (state, {data}) => ({...state, readme: data}),
    CLEAR_README: state => ({...state, readme: null})
};

registerReducer('repository', repository);

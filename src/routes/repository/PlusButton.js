import {connect} from 'react-redux';
import {TouchableOpacity, Clipboard} from 'react-native';
import {AppContainer, FormattedText, colors, Icon} from 'elements';
import React from 'react';
import {showMessage} from 'utils';
import {getRepository} from './selectors';
import {getThemeValue} from '../settings/selectors';
import {starProject} from './actions';
import {forkProjectPopup} from './Namespaces';

function getContainerStyle(darkTheme) {
    return {
        position: 'absolute',
        bottom: 70,
        right: 0,
        width: 200,
        borderColor: darkTheme ? colors.mainText : colors.background,
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderWidth: 1
    };
}

function Action({children, onPress}) {
    return (
        <TouchableOpacity onPress={onPress} style={{marginBottom: 3, flexDirection: 'row', alignItems: 'center'}}>
            <Icon name={'angle-right'}/>
            <FormattedText style={{textAlign: 'right', flex: 1}}>{children}</FormattedText>
        </TouchableOpacity>
    );
}

function PlusButtonComponent({darkTheme, repository, starProject, forkProjectPopup}) {
    if (!repository) {
        return null;
    }

    return (
        <AppContainer style={getContainerStyle(darkTheme)}>
            <Action onPress={starProject}>{'Star / Unstar Project'}</Action>
            <Action onPress={forkProjectPopup}>{'Fork Project'}</Action>
            <Action onPress={() => {
                Clipboard.setString(repository.ssh_url_to_repo);
                showMessage({message: 'URL copied to the clipboard!'});
            }}>{'Copy SSH url'}</Action>
            <Action onPress={() => {
                Clipboard.setString(repository.http_url_to_repo);
                showMessage({message: 'URL copied to the clipboard!'});
            }}>{'Copy HTTP url'}</Action>
        </AppContainer>
    );
}

export const PlusButton = connect(state => ({
    darkTheme: getThemeValue(state),
    repository: getRepository(state)
}), {starProject, forkProjectPopup})(PlusButtonComponent);

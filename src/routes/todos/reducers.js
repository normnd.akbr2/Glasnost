import _ from 'lodash';
import {registerReducer} from 'store';

function todos() {
    return {
        TODOS_RECEIVED: (state, {data}) => ({...state, todos: _.uniqBy([...(state.todos || []), ...data], 'id')}),
        SET_TODOS_PAGINATION: (state, {pagination}) => ({...state, pagination}),
        CLEAR_TODOS: state => ({...state, todos: []})
    };
}

registerReducer('todos', todos());

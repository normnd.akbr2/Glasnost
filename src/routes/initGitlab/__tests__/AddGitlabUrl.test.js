import React from 'react';
import {Provider} from 'react-redux';

import {getRegisteredRoutes, getRouteName} from 'router';

describe('AddGitlabUrl', () => {
    let AddGitlabUrl;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        setStore(getStore());
    });

    beforeEach(() => {
        AddGitlabUrl = getRegisteredRoutes().AddGitlabUrl;

        const {getStore} = require('store');
        store = getStore();
    });

    it('should render correctly the AddGitlabUrl page', async () => {
        const component = await asyncCreate(<Provider store={store}><AddGitlabUrl/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should change the gitlabUrl', async () => {
        const component = mount(<Provider store={store}><AddGitlabUrl/></Provider>);
        const SimpleInput = component.find('SimpleInputComponent');

        SimpleInput.props().onChangeText('http://someGitlabUrl.com');
        SimpleInput.props().value = 'http://someGitlabUrl.com';

        expect(component.html()).toMatchSnapshot();
    });

    it('should call the oauth Button', async () => {
        const component = mount(<Provider store={store}><AddGitlabUrl/></Provider>);
        const MainButton = component.find('MainButton').at(1);

        MainButton.props().onPress();

        expect(component.html()).toMatchSnapshot();
    });

    it('should navigate to AddCredentials', async () => {
        const component = mount(<Provider store={store}><AddGitlabUrl/></Provider>);
        const MainButton = component.find('MainButton').at(0);

        MainButton.props().onPress();

        expect(getRouteName(store.getState())).toEqual('AddCredentials');
    });
});

import {getData, parseLodash, setAuthType, setCurrentUrl, setData, setToken, setUrls, showMessage} from 'utils';
import {loadConfig} from 'api';
import _ from 'lodash';

import {navigate} from 'router';
import {hasFingerprintHardware} from './HardwareLogin';
import {changeHost} from '../settings/actions';

export function validLogin({validUrl, accessToken, authType}) {
    return async (dispatch) => {
        showMessage({message: 'Successfully logged in!'});
        loadConfig(validUrl, accessToken, authType);

        const dataFromStorage = await getData();
        const hasFingerprint = parseLodash(_.get(dataFromStorage, 'needsFingerprint', 'false'));
        const gitlabUrls = _.filter(parseLodash(_.get(dataFromStorage, 'gitlabUrls', '[]')), ({url}) => url !== validUrl);
        const tokens = _.filter(parseLodash(_.get(dataFromStorage, 'userToken', '[]')), ({url}) => url !== validUrl);
        const updatedUrls = [...gitlabUrls, {url: validUrl}];

        await setData('currentUrl', validUrl);
        await setData('gitlabUrls', JSON.stringify(updatedUrls));
        await setData('userToken', JSON.stringify([...tokens, {url: validUrl, accessToken, type: authType}]));

        dispatch(setUrls(updatedUrls));
        dispatch(setCurrentUrl(validUrl));
        dispatch(setToken(accessToken));
        dispatch(setAuthType(authType));

        if (await hasFingerprintHardware() && !hasFingerprint) {
            dispatch(navigate('HardwareLogin'));
        } else {
            dispatch(changeHost(validUrl));
        }
    };
}

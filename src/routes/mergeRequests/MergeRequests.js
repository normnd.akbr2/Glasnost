import React, {Component} from 'react';
import {FlatList, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {setFooter, setHeaderLeftButton, MainFooter} from 'headerFooter';
import {getPreviousRouteName, navigate, registerRoute} from 'router';
import {getLastUpdatedTime} from 'utils';
import {
    Icon,
    FormattedText,
    ItemList,
    Tabs,
    AppContainer,
    Tag,
    EmptyData,
    LoadMore
} from 'elements';

import {getClosedMergeRequests, getMergedRequests, getOpenMergeRequests} from './selectors';
import {requestMergeRequests} from './actions';
import {Footer} from '../repository/Footer';
import {MyFace} from '../repositories/MyFace';
import {Avatars} from '../issues/Avatars';

class MergeRequestsComponent extends Component {
    state = {selectedTab: 'Open'};

    componentDidMount() {
        const {requestMergeRequests, personal, projectId, setFooter, setHeaderLeftButton} = this.props;

        setHeaderLeftButton(this.headerLeftButton());
        setFooter(this.footer());
        requestMergeRequests(personal, true, projectId);
    }

    footer() {
        const {personal} = this.props;

        return personal ? <MainFooter/> : <Footer/>;
    }

    headerLeftButton() {
        const {navigate, personal} = this.props;

        return personal ? <MyFace/> : <Icon name={'angle-left'} size={25} style={{padding: 5}} onPress={() => navigate('Repository')}/>;
    }

    onScroll({nativeEvent}) {
        const {setFooter} = this.props;
        const currentOffset = nativeEvent.contentOffset.y;
        const diff = currentOffset - (this.offset || 0);

        if (diff < 0.3) {
            setFooter(this.footer());
        } else {
            setFooter();
        }

        this.offset = currentOffset;
    }

    render() {
        const {navigate, openMergeRequests, mergedMergeRequests, closedMergeRequests, requestMergeRequests, personal, projectId} = this.props;
        const {selectedTab} = this.state;
        let mergeRequests;

        if (selectedTab === 'Open') {
            mergeRequests = openMergeRequests;
        } else if (selectedTab === 'Merged') {
            mergeRequests = mergedMergeRequests;
        } else {
            mergeRequests = closedMergeRequests;
        }

        const tabs = [
            {
                id: 'Open',
                text: `Open (${_.size(openMergeRequests) >= 100 ? '99+' : _.size(openMergeRequests)})`,
                onPress: () => this.setState({selectedTab: 'Open'})
            },
            {
                id: 'Merged',
                text: `Merged (${_.size(mergedMergeRequests) >= 100 ? '99+' : _.size(mergedMergeRequests)})`,
                onPress: () => this.setState({selectedTab: 'Merged'})
            },
            {
                id: 'Closed',
                text: `Closed (${_.size(closedMergeRequests) >= 100 ? '99+' : _.size(closedMergeRequests)})`,
                onPress: () => this.setState({selectedTab: 'Closed'})
            }
        ];

        return (
            <AppContainer>
                <Tabs tabs={tabs} selectedTab={selectedTab}/>
                <AppContainer>
                    {!_.size(mergeRequests) && <EmptyData/>}
                    <FlatList
                        ListFooterComponent={<LoadMore entityName={'merge_requests'} onPress={() => requestMergeRequests(personal, true, projectId)}/>}
                        keyExtractor={item => `${item.id}`}
                        refreshing={false}
                        onRefresh={() => requestMergeRequests(personal, true, projectId)}
                        onScroll={e => this.onScroll(e)}
                        data={mergeRequests}
                        renderItem={({item}) => <SingleMergeRequest navigate={navigate} mr={item}/>}/>
                </AppContainer>
            </AppContainer>
        );
    }
}

function SingleMergeRequest({navigate, mr}) {
    const {assignee, author, iid, project_id} = mr;

    return (
        <ItemList onPress={() => navigate('MergeRequest', {iid, projectId: project_id})}>
            <Avatars assignee={assignee} author={author}/>
            <Description mr={mr}/>
            <Infos mr={mr}/>
        </ItemList>
    );
}

function Description({mr}) {
    const {title, updated_at} = mr;
    const {unit, value} = getLastUpdatedTime(updated_at);

    return (
        <View style={{flexDirection: 'column', flex: 1, marginLeft: 10}}>
            <FormattedText style={{flex: 1, fontWeight: 'bold'}}>{title}</FormattedText>
            <FormattedText>{`Last update: ${value} ${unit} ago`}</FormattedText>
        </View>
    );
}

function Infos({mr}) {
    const {upvotes, downvotes, labels} = mr;

    return (
        <View style={{flexDirection: 'column', alignItems: 'flex-end'}}>
            <Thumbs upvotes={upvotes} downvotes={downvotes}/>
            {_.map(labels, (label, index) => <View key={index} style={{marginBottom: 3}}><Tag text={label}/></View>)}
        </View>
    );
}

function Thumbs({upvotes, downvotes, size = 15}) {
    return (
        <View style={{flexDirection: 'row'}}>
            <View style={{flexDirection: 'row', marginRight: 5}}>
                <Icon name={'thumb-up'} size={size}/>
                <FormattedText style={{fontSize: size}}>{upvotes || 0}</FormattedText>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Icon name={'thumb-down'} size={size}/>
                <FormattedText style={{fontSize: size}}>{downvotes || 0}</FormattedText>
            </View>
        </View>
    );
}

const MergeRequests = connect(state => ({
    openMergeRequests: getOpenMergeRequests(state),
    mergedMergeRequests: getMergedRequests(state),
    closedMergeRequests: getClosedMergeRequests(state),
    previousRouteName: getPreviousRouteName(state)
}), {
    requestMergeRequests, navigate, setFooter, setHeaderLeftButton
})(MergeRequestsComponent);
registerRoute({MergeRequests});

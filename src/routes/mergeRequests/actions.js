import _ from 'lodash';
import {get} from 'api';
import {getPagination, setPagination} from 'utils';
import {getRepository} from '../repository/selectors';

function requestStateMergeRequests({personal, state, page0, project_id}) {
    return async (dispatch, getState) => {
        let mergeRequests;

        const {pageToLoad = 0} = getPagination(getState(), 'merge_requests');

        if (personal) {
            mergeRequests = await get({
                path: 'merge_requests',
                additionalParams: {per_page: 100, scope: 'assigned_to_me', state},
                page: page0 ? 0 : pageToLoad
            });
        } else {
            const repo = getRepository(getState());
            if (_.isEmpty(repo) && !project_id) {
                return;
            }

            const repoId = _.get(repo, 'id', project_id);

            mergeRequests = await get({
                path: `projects/${repoId}/merge_requests`,
                additionalParams: {per_page: 100, state},
                page: page0 ? 0 : pageToLoad
            });
        }

        if (mergeRequests.data) {
            dispatch({type: 'MERGE_REQUESTS_RECEIVED', data: mergeRequests.data});
            dispatch(setPagination('merge_requests', mergeRequests.headers));
        }
    };
}

export function requestMergeRequests(personal, page0, project_id) {
    return (dispatch) => {
        dispatch(requestStateMergeRequests({personal, state: 'opened', page0, project_id}));
        dispatch(requestStateMergeRequests({personal, state: 'merged', page0, project_id}));
        dispatch(requestStateMergeRequests({personal, state: 'closed', page0, project_id}));
    };
}

export function clearMergeRequests() {
    return dispatch => dispatch({type: 'CLEAR_MERGE_REQUESTS'});
}

import _ from 'lodash';

import {get, post, put} from 'api';

import {getRepository} from '../repository/selectors';
import {getIssue} from './selectors';
import {requestRepository} from '../repository/actions';

function requestDiscussions() {
    return async (dispatch, getState) => {
        const issue = getIssue(getState());

        if (_.isEmpty(issue)) {
            return;
        }

        const {data} = await get({
            path: `projects/${issue.project_id}/issues/${issue.iid}/discussions`,
            additionalParams: {per_page: 100}
        });

        if (data) {
            dispatch({type: 'ISSUE_DISCUSSIONS_RECEIVED', data});
        }
    };
}

export function requestIssue(iid, project_id) {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());

        if (_.isEmpty(repo) && !project_id) {
            return;
        }

        const projectId = project_id || _.get(repo, 'id');
        const {data} = await get({path: `projects/${projectId}/issues/${iid}`});

        if (data) {
            dispatch({type: 'ISSUE_RECEIVED', data});
            dispatch(requestRepository(projectId));
            dispatch(requestDiscussions());
        }
    };
}

export function editIssue(changes) {
    return async (dispatch, getState) => {
        const issue = getIssue(getState());

        if (_.isEmpty(issue)) {
            return;
        }

        await put({path: `projects/${issue.project_id}/issues/${issue.iid}`, body: changes});

        dispatch(requestIssue(issue.iid));
    };
}

export function commentIssue(body) {
    return async (dispatch, getState) => {
        const issue = getIssue(getState());

        if (_.isEmpty(issue)) {
            return;
        }

        await post({path: `projects/${issue.project_id}/issues/${issue.iid}/discussions`, body});

        dispatch(requestDiscussions());
    };
}

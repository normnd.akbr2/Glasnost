import React, {Component} from 'react';
import _ from 'lodash';
import {connect} from 'react-redux';
import {FlatList, View} from 'react-native';

import {navigate} from 'router';
import {Card} from 'elements/card';
import {FormattedText} from 'elements/text';
import {ItemList} from 'elements/itemList';

import {getEvents, getOtherGuyEvents} from './selectors';
import {getActionMessage, requestEvents} from './actions';

class EventsComponent extends Component {
    componentDidMount() {
        const {id, requestEvents} = this.props;

        requestEvents(id);
    }

    render() {
        const {events, getActionMessage, navigate} = this.props;

        if (_.isEmpty(events)) {
            return null;
        }

        return (
            <Card title={'Last events'} icon={{name: 'log'}}>
                <FlatList
                    data={events}
                    keyExtractor={item => item.created_at}
                    renderItem={({item}) => <SingleEvent
                        event={item}
                        getActionMessage={getActionMessage}
                        navigate={navigate}
                    />}
                />
            </Card>
        );
    }
}

class SingleEvent extends Component {
    state = {message: ''};

    async componentDidMount() {
        const {getActionMessage, event} = this.props;
        const {message, repoName} = await getActionMessage(event);

        this.setState({message, repoName});
    }

    render() {
        const {event, navigate} = this.props;
        const {message, repoName} = this.state;

        const project_id = _.get(event, 'project_id');

        if (!message) {
            return null;
        }

        return (
            <ItemList onPress={() => navigate('Repository', {id: project_id})}>
                <View style={{flex: 1}}>
                    <FormattedText style={{fontWeight: 'bold'}}>{repoName}</FormattedText>
                    <View style={{marginHorizontal: 15}}>
                        <FormattedText>{_.truncate(message, {length: 150})}</FormattedText>
                    </View>
                </View>
            </ItemList>
        );
    }
}

export const Events = connect((state, {personal = false}) => ({
    events: personal ? getEvents(state) : getOtherGuyEvents(state)
}), {requestEvents, getActionMessage, navigate})(EventsComponent);

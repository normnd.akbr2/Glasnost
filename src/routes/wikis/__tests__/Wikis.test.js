import React from 'react';
import {Provider} from 'react-redux';
import {getRegisteredRoutes, getRouteName, navigate} from 'router';
import _ from 'lodash';

import {getHeaderRightButton} from 'headerFooter/selectors';
import {clearWikis} from '../actions';
import {setLocalSearch} from '../../repositories/actions';
import {getWikis} from '../selectors';

const fakeWikis = [
    {id: 1, title: 'someTitle', slug: 'someSlug'},
    {id: 2, title: 'someTitle2', slug: 'someSlug'},
    {id: 3, title: 'someTitle3', slug: 'someSlug'}
];

describe('Wikis', () => {
    let Wikis;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../repository/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        Wikis = getRegisteredRoutes().Wikis;

        store = require('store').getStore();
        store.dispatch(clearWikis());

        // populate navigation's history
        store.dispatch(navigate('Repository'));
        store.dispatch(navigate('Glasnost'));

        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /wikis*/, data: fakeWikis});
    });

    it('renders correctly the empty page', async () => {
        const component = await asyncCreate(<Provider store={store}><Wikis/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should not render because there is no repository', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: null});
        const component = await asyncCreate(<Provider store={store}><Wikis/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should not render because there is no mergeRequests', async () => {
        onGet({url: /merge_requests*/, data: null});
        const component = await asyncCreate(<Provider store={store}><Wikis/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the list', async () => {
        const component = await asyncCreate(<Provider store={store}><Wikis/></Provider>);
        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the personal list', async () => {
        const component = await asyncCreate(<Provider store={store}><Wikis personal={true}/></Provider>);
        expect(component.toJSON()).toMatchSnapshot();
    });

    it('refreshes the list', async () => {
        const component = mount(<Provider store={store}><Wikis/></Provider>);
        await waitForAsync();
        component.update();

        const ScrollableAppContainer = component.find('ScrollableAppContainerComponent').at(0);
        ScrollableAppContainer.props().refresh();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should call the functions when unmounting', async () => {
        const component = mount(<Provider store={store}><Wikis/></Provider>);
        await waitForAsync();
        component.update();

        component.unmount();
        await waitForAsync();

        expect(_.isEmpty(getWikis(store.getState()))).toBeTruthy();
    });

    it('should ask for more merge_requests when clicking the button', async () => {
        const component = mount(<Provider store={store}><Wikis/></Provider>);
        await waitForAsync();
        component.update();

        const LoadMore = component.find('LoadMoreComponent');
        LoadMore.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should navigate to Wiki when clicking on a single wiki', async () => {
        const component = mount(<Provider store={store}><Wikis/></Provider>);
        await waitForAsync();
        component.update();

        const ItemList = component.find('ItemListComponent').at(0);
        ItemList.props().onPress();
        await waitForAsync();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Wiki');
    });

    it('should toggle the search bar', async () => {
        store.dispatch(setLocalSearch(true));
        const component = mount(<Provider store={store}><Wikis/></Provider>);
        await waitForAsync();
        component.update();

        const SearchButton = getHeaderRightButton(store.getState());
        SearchButton.props.onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should write in the search bar', async () => {
        store.dispatch(setLocalSearch(true));
        const component = mount(<Provider store={store}><Wikis/></Provider>);
        await waitForAsync();
        component.update();

        const SearchButton = getHeaderRightButton(store.getState());
        SearchButton.props.onPress();
        component.update();

        const SearchBar = component.find('TextInput').at(0);
        SearchBar.props().onChangeText('someMessage2');
        SearchBar.props().value = 'someMessage2';
        component.update();

        expect(component.html()).toMatchSnapshot();
    });
});
